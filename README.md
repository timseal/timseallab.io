![Build Status](https://gitlab.com/pages/nanoc/badges/master/build.svg)


---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci]. Magically.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Generate the website: `bundle exec nanoc`
1. Preview your project: `bundle exec nanoc view`
1. Add content (this is the tricky bit)

Read more at Nanoc's [documentation](https://nanoc.ws/doc/)

## GitLab User or Group Pages

Accessible at [https://timseal.gitlab.io/][generatedsite]

## Troubleshooting

1. No trouble!

[ci]: https://about.gitlab.com/gitlab-ci/
[nanoc]: http://nanoc.ws/
[install]: http://nanoc.ws/doc/installation/
[documentation]: http://nanoc.ws/doc/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
[generatedsite]: https://timseal.gitlab.io/
